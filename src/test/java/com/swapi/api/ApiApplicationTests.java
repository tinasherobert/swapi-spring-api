package com.swapi.api;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@EnableWebMvc
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ApiApplicationTests {

    @Autowired
    MockMvc mockMvc;


    @Test
    @Order(0)
    void ListFirstPageOfEmptyStringSearch() throws Exception {
        MvcResult mvcResult = mockMvc.perform(post("/graphql")
                .content("{\"query\":\"query {\\n  people(searchQuery: \\\"\\\", page: \\\"1\\\") {\\n    count\\n    next\\n    previous\\n    results {\\n      gender\\n      height\\n      homeworld\\n      mass\\n      name\\n    }\\n  }\\n}\\n\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(request().asyncStarted())
                .andExpect(request().asyncResult(notNullValue()))
                .andReturn();

        mockMvc.perform(asyncDispatch(mvcResult))
                .andDo(print())
                .andExpect(jsonPath("$.data", is(anything())))
                .andExpect(status().isOk());

    }

   @Test
    @Order(1)
    void SearchForAPerson() throws Exception {

       MvcResult mvcResult = mockMvc.perform(post("/graphql")
               .content("{\"query\":\"query {\\n  people(searchQuery: \\\"Luke\\\", page: \\\"1\\\") {\\n    count\\n    next\\n    previous\\n    results {\\n      gender\\n      height\\n      homeworld\\n      mass\\n      name\\n    }\\n  }\\n}\\n\"}")
               .contentType(MediaType.APPLICATION_JSON))
               .andExpect(request().asyncStarted())
               .andExpect(request().asyncResult(notNullValue()))
               .andReturn();

       mockMvc.perform(asyncDispatch(mvcResult))
               .andDo(print())
               .andExpect(jsonPath("$.data", is(anything())))
               .andExpect(status().isOk());

    }

    @Test
    @Order(0)
    void ViewAPersonById() throws Exception {
        MvcResult mvcResult = mockMvc.perform(post("/graphql")
                .content("{\"query\":\"query {\\n  person(personId: 1) {\\n    gender\\n    height\\n    homeworld\\n    mass\\n    name\\n  }\\n}\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(request().asyncStarted())
                .andExpect(request().asyncResult(notNullValue()))
                .andReturn();

        mockMvc.perform(asyncDispatch(mvcResult))
                .andDo(print())
                .andExpect(jsonPath("$.data", is(anything())))
                .andExpect(status().isOk());
    }

}
