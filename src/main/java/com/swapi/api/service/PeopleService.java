package com.swapi.api.service;

import com.swapi.api.model.PeopleData;
import com.swapi.api.model.Result;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
@GraphQLApi
@Slf4j
public class PeopleService {


    private final RestTemplate restTemplate;
    @Value("${swapi.url.people}")
    private String peopleDataUrl;
    @Value("${swapi.url.person}")
    private String personDataUrl;

    public PeopleService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GraphQLQuery(name = "people") // READ ALL PEOPLE
    public PeopleData getAllPeople(@GraphQLArgument(name = "page" ,defaultValue = "0") String page , @GraphQLArgument(name = "searchQuery" , defaultValue = "") String searchQuery) {
        Map<String, String> params = new HashMap<>();
        params.put("page", page);
        params.put("searchQuery", searchQuery);
        PeopleData resultPeople = restTemplate.getForObject(peopleDataUrl, PeopleData.class, params);
        log.info("people: {}", resultPeople);
        return resultPeople;
    }

    @GraphQLQuery(name = "person") // READ PERSON BY ID
    public Result getPersonById(@GraphQLArgument(name = "personId") Long personId) {
        Map<String, Long> params = new HashMap<>();
        params.put("personId", personId);
        Result resultPerson = restTemplate.getForObject(personDataUrl, Result.class, params);
        log.info("person: {}", resultPerson);
        return resultPerson;
    }


}
