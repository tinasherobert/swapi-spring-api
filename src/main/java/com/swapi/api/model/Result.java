package com.swapi.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result {
    String name ;
    String height ;
    String mass ;
    String gender ;
    String homeworld ;
}
