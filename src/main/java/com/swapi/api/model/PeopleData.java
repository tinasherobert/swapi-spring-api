package com.swapi.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PeopleData implements Serializable {
    int count ;
    String next ;
    String previous ;
    List<Result> results;
}
