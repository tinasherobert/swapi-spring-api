
#SWAPI Graphql Api
To access the graphql client gui use the following link 

    http://localhost:9000/gui
    
An example of the post request to the graphql server api
1. Listing all the people on page 1 

        curl 'http://localhost:9000/graphql' -H 'Accept-Encoding: gzip, deflate, br' -H 'Content-Type: application/json' -H 'Accept: application/json' -H 'Connection: keep-alive' -H 'DNT: 1' -H 'Origin: http://localhost:9000' --data-binary '{"query":"query{\n  people(searchQuery: \"\", page: \"1\") {\n    count\n    next\n    previous\n    results {\n      gender\n      height\n      homeworld\n      mass\n      name\n    }\n  }\n}"}' --compressed
    
2. Search for a person 

        curl 'http://localhost:9000/graphql' -H 'Accept-Encoding: gzip, deflate, br' -H 'Content-Type: application/json' -H 'Accept: application/json' -H 'Connection: keep-alive' -H 'DNT: 1' -H 'Origin: http://localhost:9000' --data-binary '{"query":"query{\n  people(searchQuery: \"Luke\", page: \"1\") {\n    count\n    next\n    previous\n    results {\n      gender\n      height\n      homeworld\n      mass\n      name\n    }\n  }\n}"}' --compressed
    
3. View a person 

        curl 'http://localhost:9000/graphql' -H 'Accept-Encoding: gzip, deflate, br' -H 'Content-Type: application/json' -H 'Accept: application/json' -H 'Connection: keep-alive' -H 'DNT: 1' -H 'Origin: http://localhost:9000' --data-binary '{"query":"query{\n  person(personId: 1) {\n    gender\n    height\n    homeworld\n    mass\n    name\n  }\n}"}' --compressed
        
    